#include <iostream>;
#include <time.h>;
#include <Windows.h>

int main()
{
	const int N = 10;  // ������ �������

	int array[N][N]{};

	struct tm LocalTime;  // ��������� �������, �� ������� ����� ���������� �����, �����, ��� � � �...
	time_t osTime = time(NULL);  // �������� ��������� ����� � ��������
	localtime_s(&LocalTime, &osTime);  // ����������� ��������� ����� � ��������� � ������������� ��������� � LocalTime

	int today_day = LocalTime.tm_mday, today_month = LocalTime.tm_mon + 1, today_year = LocalTime.tm_year + 1900,
		summa = 0;

	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			array[i][j] = i + j;
			if (i == today_day % N)
				summa += array[i][j];
		}
	}

	HANDLE Console = GetStdHandle(STD_OUTPUT_HANDLE);

	for (int i = 0; i < N; i++)
	{
		if (i != 0)
		{
			std::cout << std::endl;
		}
		for (int j = 0; j < N; j++)
		{
			if (i == today_day % N) { SetConsoleTextAttribute(Console, 10); }
			else { SetConsoleTextAttribute(Console, 15); }
			std::cout.width(3);
			std::cout << array[i][j];
		}
	}

	std::cout << std::endl;


	std::cout << "\nToday is: " << today_day << '-' << today_month << '-' << today_year << std::endl;
	std::cout << "\nArray's size is: " << N << 'x' << N << std::endl;
	std::cout << "\nResult of operation (today_day % N) is: " << today_day << " % " << N << " = " << today_day % N << std::endl;
	std::cout << "\nThe sum of " << today_day % N << " row is: " << summa << std::endl;
	
	return 0;
}

